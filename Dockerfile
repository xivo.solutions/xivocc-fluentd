FROM fluent/fluentd:v1.16-debian-1

# Use root account to use apt
USER root

RUN buildDeps="sudo make gcc g++ libc-dev" \
 && apt-get update \
 && apt-get install -y --no-install-recommends $buildDeps libpq-dev postgresql-client\
 && fluent-gem install fluent-plugin-postgres \
 && fluent-gem install fluent-plugin-record-modifier --no-document \
 && sudo gem sources --clear-all \
 && SUDO_FORCE_REMOVE=yes \
    apt-get purge -y --auto-remove \
                  -o APT::AutoRemove::RecommendsImportant=false \
                  $buildDeps \
 && rm -rf /var/lib/apt/lists/* \
 && rm -rf /tmp/* /var/tmp/* /usr/lib/ruby/gems/*/cache/*.gem

COPY /fluentd/fluent.conf /fluentd/etc/

USER fluent

ARG TARGET_VERSION
LABEL version=${TARGET_VERSION}