# Fluentd - XIVO XUC logs parser

## Purpose

The aim of the project is to create a graphs about the audio quality issues of the users thanks to the data that we can find in logs.

Fluentd is a tools that allow us to parse logs.
Currently we use fluentd to parse XUC logs to retreive call quality issues data (ie: RTT, jitter UP/DOWN, Packet loss UP/DOWN)
Then fluentd pushes the data into xivo_db and thanks to Grafana ( project link : xivo-grafana ) we are able to dipslay graphs.

[ diagram ]

1. We need to add in the /var/lib/../pg_hba.conf the authorisation for the postgres user to access the callstats database from the xivocc

## Debug / Test

1. Build the dockerfile image

    ```bash
    docker build -t <image_name> <dockerfile_directory>
    ```
1. In the docker-compose.yml file you need to specify the same image name : "image: <image_name>". Then :
    ```
    cd <directory_to_docker-compose.yml>
    docker-compose up -d
    ```

1. Start the docker containers

    ```bash
    cd <directory_to_docker-compose.yml>
    docker-compose up -d
    ```
1. [ OPTIONNAL ] if you want to change Fluentd configuration file edit ./conf/fluent.conf
    You can find more information about fluend config file on the official documentation: https://docs.fluentd.org/

